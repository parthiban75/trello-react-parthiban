import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import "./component/styles.css"
import Navbar from './component/Navbar';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import BoardItems from './component/BoardItems';
import BoardsContainer from './component/BoardsContainer';
import MainBoxContainer from './component/MainBoxContainer';




function App() {
  return (
    <Router>
      <div className="App">
        <div className="nav-bar">
          <Navbar></Navbar>

        </div>
        <MainBoxContainer></MainBoxContainer>
        <hr></hr>
        <div className='boardsDetail'>

        </div>
        <div className="col-10 main-container" >
          <Switch>
            <Route exact path="/" component={BoardsContainer} />
            <Route path="/:id" component={BoardItems} />
          </Switch>
        </div>




      </div>

    </Router>




  );
}


export default App;
