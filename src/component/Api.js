import axios from 'axios'

axios.defaults.baseURL = "https://api.trello.com/1/"





axios.defaults.params = {
    key: "6dd6b79fcefe52ebd9a73b91b7258f5c",
    token: "ATTA71f12e78fd0a3cf40da47ff3c91a606da3d7893685edd5a7be68a6501f851bf8787268A1"
}


export function getBoards() {
    return axios.get(`members/me/boards`)
}
export function createBoard(boardName) {
    return axios.post(`boards/?name=${boardName}`)
}

export function getList(boardId) {
    return axios.get(`boards/${boardId}/lists/`)
}
export function createList(boardId, name) {
    return axios.post(`boards/${boardId}/lists?name=${name}`)
}
export function archiveList(listId) {
    return axios.put(`lists/${listId}/?closed=true`)
}
export function getCard(listId) {
    return axios.get(`lists/${listId}/cards`)
}
export function createCard(listId, name) {
    return axios.post(`cards?idList=${listId}&name=${name}`)
}
export function deleteCard(cardId) {
    return axios.delete(`cards/${cardId}`)
}


