import React, { Component } from 'react'

import ListItems from './ListItems'
import { createList, archiveList, getList } from './Api'

class BoardItems extends Component {
    constructor(props) {
        super(props)

        this.state = {
            lists: [],
            errorMsg: "",
            hasLoader: true,
            listName: ""
        }
    }
    componentDidMount() {
        console.log(this.props)
        const boardId = this.props.match.params.id
        const fetchLists = async() => {
            try{
            const response = await getList(boardId)
            this.setState({ lists: response.data})
            } catch {
                this.setState({ errorMsg: "Error while retrieving boards "})
            }
            this.setState({hasLoader : false})
        }
        fetchLists()
    }
    handleListName = (event) => {
        this.setState({ listName: event.target.value })
    }
    addList = (event) => {
        event.preventDefault()
        const fetchLists = async() =>{
            this.setState({ hasLoader: true })
            try{
            const newList = await createList(this.props.match.params.id, event.target.name.value)
            this.setState({ lists: [...this.state.lists, newList.data]})
            } catch {
                this.setState({ errorMsg: " Error while adding a new List"})
            }
            this.setState({listName : "" , hasLoader: false})
        }
        fetchLists()
    }
    archiveList = (listToBeArchive) => {
        console.log(listToBeArchive.id)
        const fetchLists = async() => {
            this.setState({ hasLoader: true })
            try{
                await archiveList(listToBeArchive.id)
                this.setState({ lists: this.state.lists.filter(list => list.id !== listToBeArchive.id)})
            } catch {
                this.setState({ errorMsg: " Error while archiving a List"})
            }
            this.setState({hasLoader: false})
        }
        fetchLists()
    }
    render() {
        const { lists, errorMsg, hasLoader } = this.state
        return (
            <div className='lists' style={{ width: "max-content" }}>
                {
                    hasLoader && <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                }
                {
                    lists.length ?
                        lists.map(list => {
                            return <ListItems key={list.id} list={list} onArchive={this.archiveList} />
                        })
                        : "No list to display.Add new list"
                }
                <div>
                    <form onSubmit={this.addList} className='add-a-list'>
                        <input type="text" name='name' placeholder='Add a list' className='list-text' value={this.state.listName} required onChange={this.handleListName} />
                        <button type='submit' className='btn btn-primary'>Add List</button>
                    </form>
                </div>

                {
                    errorMsg ? <div>{errorMsg}</div> : null
                }
            </div>
        )
    }
}

export default BoardItems

