import React, { Component } from 'react'

class Sidebar extends Component {
  render() {
    return (
        <nav
        id="sidebarMenu"
        class="collapse d-lg-block sidebar collapse bg-white"
      >
        <div class="position-sticky " >
          <div class="list-group list-group-flush">
            <a
              href="#"
              class="list-group-item list-group-item-action py-2 ripple"
              aria-current="true"
            >
              <i class="fas fa-tachometer-alt fa-fw me-3 mt-0"></i
              ><span>Main dashboard</span>
            </a>
            <a
              href="#"
              class="list-group-item list-group-item-action py-2 ripple"
            ><i class="fas fa-chart-line fa-fw me-3"></i
            ><span>Boards</span></a
            >

            <a
              href="#"
              class="list-group-item list-group-item-action py-2 ripple"
            ><i class="fas fa-lock fa-fw me-3"></i><span>Members</span></a
            >
            <a
              href="#"
              class="list-group-item list-group-item-action py-2 ripple"
            ><i class="fas fa-chart-line fa-fw me-3"></i
            ><span>Settings</span></a
            >
            <a
              href="#"
              class="list-group-item list-group-item-action py-2 ripple"
            >
              <i class="fas fa-chart-pie fa-fw me-3"></i><span>Table</span>
            </a>
            <a
              href="#"
              class="list-group-item list-group-item-action py-2 ripple"
            ><i class="fas fa-chart-bar fa-fw me-3"></i><span>Calendar</span></a
            >


          </div>
        </div>
      </nav>

    )
  }
}

export default Sidebar