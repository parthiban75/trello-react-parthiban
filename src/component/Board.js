import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import "./styles.css"
class Board extends Component {
  render() {
    const { board } = this.props
    
    return (
      <div className='board ' >
        <Link to={`/${board.id}`} style={{ textDecoration: "none", width: "100%", height: "100%" }} > <div className='board-name'>{board.name}</div></Link>
      </div>
    )
  }
}

export default Board
