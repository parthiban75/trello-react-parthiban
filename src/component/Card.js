import React, { Component } from 'react'

export class Card extends Component {
  render() {
    return (
      <div>
        <h6 className='card-name'> {this.props.card.name}
        <div className='btn btn-danger btn-sm delete-card' style={{padding:"0px 2px"}} onClick={()=>{this.props.onDelete(this.props.card)}}>X</div></h6>
      </div>
    )
  }
}

export default Card
