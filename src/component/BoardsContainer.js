import React, { Component } from 'react'
import Board from './Board'
import "./styles.css"
import { getBoards, createBoard } from './Api'

class BoardsContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            boards: [],
            errorMsg: "",
            hasLoader: true,
            boardName: ""
        }
    }
    componentDidMount() {
        const fetchBoards = async () => {
            try {
                const response = await getBoards()
                console.log(response.data)
                this.setState({ boards: response.data, hasLoader: false })
            }
            catch {
                this.setState({ errorMsg: "Error while retrieving boards ", hasLoader: false })
            }
        }
        fetchBoards()
    }
    handleBoardName = (event) => {
        this.setState({ boardName: event.target.value })
    }


    handleChange = (event) => {
        this.setState({
            boardName: event.target.value
        })
    }
    createBoard = (event) => {
        event.preventDefault()
        const fetchBoards = async () => {
            this.setState({ hasLoader: true })
            try {
                const newBoard = await createBoard(event.target.name.value)
                this.setState({ boards: [...this.state.boards, newBoard.data], boardName: " " })
            } catch {
                this.setState({ errorMsg: "Error while deleting a boards ", boardName: "" })
            }
            this.setState({ hasLoader: false })
        }
        fetchBoards()
    }


    render() {
        const { errorMsg, boards, hasLoader } = this.state
        return (
            <div>
                <div className='create-board container col-md-4 col-lg-8'>
                    <form onSubmit={this.createBoard}>
                        <label>Create a new board : </label>
                        <input value={this.state.boardName} onChange={this.handleChange} type="text" name='name' placeholder='Enter name of a board' className='board-text' required />
                        <button type='submit' className='btn btn-primary add-board-button' >Add board</button>
                    </form>
                </div>
                {
                    hasLoader && <div className="lds-ellipsis"><div></div><div></div><div></div></div>
                }
                <div className='container boards'>
                    {
                        boards.length ?
                            boards.map((board) => {
                                return <Board key={board.id} board={board} />
                            }) : "No boards to display"
                    }
                    {
                        errorMsg ? <div>{errorMsg}</div> : null
                    }
                </div>

            </div>
        )
    }
}

export default BoardsContainer
